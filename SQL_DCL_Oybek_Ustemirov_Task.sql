CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;



GRANT SELECT ON TABLE customer TO rentaluser;
SELECT * FROM customer;



CREATE GROUP rental;
ALTER GROUP rental ADD USER rentaluser;


GRANT INSERT, UPDATE ON TABLE rental TO rental;
INSERT INTO rental (column1, column2, ...) VALUES (value1, value2, ...);


select 	* from rental r order by r.rental_id desc;
-- Grant INSERT and UPDATE permissions to the "rental" group for the "rental" table
GRANT INSERT, UPDATE ON TABLE rental TO rental;

-- Insert a new row into the "rental" table
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update) 
VALUES ('32295', current_date,'367', '130', current_date, '1', current_date);

-- Update an existing row in the "rental" table
UPDATE rental SET inventory_id = '400' WHERE rental_id = '32295';

select * from rental r where r.rental_id in ('32295','32296');



REVOKE INSERT ON TABLE rental FROM rental;
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update) 
VALUES ('32296', current_date,'367', '130', current_date, '1', current_date);


select * from customer c 

DO $$ 
DECLARE
    customer_row RECORD;
BEGIN
    FOR customer_row IN SELECT customer_id, first_name, last_name
                       FROM customer
    LOOP
        EXECUTE FORMAT('CREATE ROLE client_%I_%I', customer_row.first_name, customer_row.last_name);
    END LOOP;
END $$;
